"""Methods for processing sound (sound processors)"""

import math

import soundfile

import config

def ampli(sound, factor: float):
    """Amplify signal (sound) by the specified factor

    :param sound:  sound (list of samples) to amplify
    :param factor: amplification factor
    :return:     list of samples (sound)
    """

    out_sound = [0]* len(sound)
    for nsample in range(len(sound)):
        value = sound[nsample] * factor
        if value > config.max_amp:
            value = config.max_amp
        elif value < - config.max_amp:
            value = -config.max_amp
        out_sound[nsample] = int(value)
    return out_sound

def shift (sound, value:int):
    "Suma una cantidad (value) a todos los elementos de la lista"
    for i in range (len(sound)):
        sound[i] += value
    return sound
def trim(sound, reduction: int, start: bool):
    """Elimina de la lista la cantidad indicada por reduction, desde el principio o del final dependiendo de start"""
    if start:
        sound = sound[reduction:]
    else:
        sound = sound[:-reduction]
    return sound
 #posible cambio en la funcion trim probar!!1

def repeat(sound, factor: int):
    "Crea una lista con la cantidad de copias que indica el factor más la suma de la original"
    sound*= (factor+1)
    return sound

